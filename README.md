## Search Page
This project implements a simple search page using HTML5 with CSS3 and vanilla 
JavaScript. The app uses 
[Search JSON API](https://developers.google.com/custom-search/v1/overview) 
by Google which is limited to 100 API calls per day as maximum. 

## Screenshots
The web page has responsive design. 


<img src="/doc/desktop_landing.png" width="800">


<img src="/doc/desktop_search.png" width="800">


<img src="/doc/small_laptop_search.png" width="800"> 


<img src="/doc/iPad_search.png" width="600">


<img src="/doc/iPhone_landing.png" width="400"> <br/>


<img src="/doc/iPhone_search.png" width="400">
