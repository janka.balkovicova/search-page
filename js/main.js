"use strict";

/* ==========================================================================
  Constants
  ========================================================================== */

const API_KEY = "AIzaSyCLfHj8Y6NH8vkjDI5B_f-ueRpaXZJLjMk";
const CX_ID = "003601618108564394494:dporunw8awq";

const IMG_RESULTS_CNT = 9;
const WEB_RESULTS_CNT = 10;

const webResultsElement = document.querySelector(".web-results > .results");
const imgResultsElement = document.querySelector(".img-results > .results");

const webPageNumber = document.querySelector(
  ".web-results > .pagination > .page-number"
);
const imgPageNumber = document.querySelector(
  ".img-results > .pagination > .page-number"
);

/* ==========================================================================
  Search
  ========================================================================== */

let searchExpression = "";
let imgStart = 1;
let webStart = 1;

function search(e) {
  e.preventDefault();
  removeSearchScreen();
  cleanSearchResults();

  searchExpression = document.getElementById("search-input").value;
  if (searchExpression) {
    getWebsites();
    getImages();
  }
}

function removeSearchScreen() {
  if (document.getElementsByClassName("hidden").length > 0) {
    removeElementClass("#container", "just-search");
    removeElementClass("#search-bar", "search-col");
    removeElementClass("#results", "hidden");
  }
}

function cleanSearchResults() {
  cleanChildNodes(webResultsElement);
  cleanChildNodes(imgResultsElement);

  imgStart = 1;
  imgPageNumber.innerText = imgStart;
  webStart = 1;
  webPageNumber.innerText = webStart;
}

function createNoWebsitesFoundMessage(forWhat = "websites", message = "") {
  if (forWhat === "websites") {
    webResultsElement.innerHTML = message ? message : "No websites found :(";
    webResultsElement.classList.add("no-results");
  } else if (forWhat === "images") {
    imgResultsElement.innerHTML = message ? message : "No images found :(";
    imgResultsElement.classList.add("no-results");
  }
}

document.getElementById("search-bar").addEventListener("submit", search, false);

/* ==========================================================================
  Websites search
  ========================================================================== */

function getWebsites() {
  fetch(
    `https://www.googleapis.com/customsearch/v1?key=${API_KEY}&cx=${CX_ID}&q=${searchExpression}&num=${WEB_RESULTS_CNT}&start=${webStart}`
  )
    .then(response => {
      if (!response.ok) {
        throw new Error("HTTP error " + response.status);
      }
      return response.json();
    })
    .then(responseData => processWebsiteResults(responseData))
    .catch(error => createNoWebsitesFoundMessage("websites", error));
}

function processWebsiteResults(responseData) {
  if (
    responseData.searchInformation.totalResults - webStart < WEB_RESULTS_CNT ||
    webStart >= 100
  )
    addElementClass(".web-results > .pagination > .next-button", "invisible");

  if (responseData.searchInformation.totalResults > 0) {
    if (webResultsElement.classList.contains("no-results"))
      webResultsElement.classList.remove("no-results");

    const websiteResults = formatWebsiteResults(responseData.items);

    websiteResults.forEach(result => {
      let websiteNode = createWebsiteNode(result);
      webResultsElement.append(websiteNode);
    });
  } else {
    createNoWebsitesFoundMessage();
  }
}

function formatWebsiteResults(results) {
  return results.map(resultData => {
    return {
      title: resultData.title,
      snippet: resultData.snippet,
      url: resultData.link,
      urlDisplayed: resultData.formattedUrl
    };
  });
}

function createWebsiteNode(websiteData) {
  let result = createNode("div");
  result.classList.add("website");

  let link = createNode("a");
  link.href = websiteData.url;
  link.innerHTML = websiteData.title;
  link.classList.add("title");

  let displayedLink = createNode("a");
  displayedLink.href = websiteData.url;
  displayedLink.innerHTML = websiteData.urlDisplayed;
  displayedLink.classList.add("displayed-link");

  let snippet = createNode("p");
  snippet.innerHTML = websiteData.snippet;
  snippet.classList.add("snippet");

  result.append(link);
  result.append(createNode("br"));
  result.append(displayedLink);
  result.append(snippet);
  return result;
}

function webPrevPage() {
  if (webStart - WEB_RESULTS_CNT <= WEB_RESULTS_CNT)
    addElementClass(".web-results > .pagination > .prev-button", "invisible");

  if (elementHasClass(".web-results > .pagination > .next-button", "invisible"))
    removeElementClass(
      ".web-results > .pagination > .next-button",
      "invisible"
    );

  webStart -= WEB_RESULTS_CNT;
  webPageNumber.innerText = `${+webPageNumber.innerText - 1}`;
  cleanChildNodes(webResultsElement);
  getWebsites();
}

function webNextPage() {
  if (webStart <= 1)
    removeElementClass(
      ".web-results > .pagination > .prev-button",
      "invisible"
    );
  webStart += WEB_RESULTS_CNT;
  webPageNumber.innerText = `${+webPageNumber.innerText + 1}`;
  cleanChildNodes(webResultsElement);
  getWebsites();
}

/* ==========================================================================
  Image search
  ========================================================================== */

function getImages() {
  fetch(
    `https://www.googleapis.com/customsearch/v1?key=${API_KEY}&cx=${CX_ID}&q=${searchExpression}&searchType=image&num=${IMG_RESULTS_CNT}&start=${imgStart}`
  )
    .then(response => {
      if (!response.ok) {
        throw new Error("HTTP error " + response.status);
      }
      return response.json();
    })
    .then(responseData => processImageResults(responseData))
    .catch(error => createNoWebsitesFoundMessage("images", error));
}

function processImageResults(responseData) {
  if (
    responseData.searchInformation.totalResults - imgStart < IMG_RESULTS_CNT ||
    imgStart >= 100
  )
    addElementClass(".img-results > .pagination > .next-button", "invisible");

  if (responseData.searchInformation.totalResults > 0) {
    if (imgResultsElement.classList.contains("no-results"))
      imgResultsElement.classList.remove("no-results");

    const imageResults = formatImageResults(responseData.items);

    imageResults.forEach(result => {
      const imageNode = createImageNode(result);
      imgResultsElement.append(imageNode);
    });
  } else {
    createNoWebsitesFoundMessage("images");
  }
}

function formatImageResults(results) {
  return results.map(resultData => {
    return {
      url: resultData.link,
      thumbnailUrl: resultData.image.thumbnailLink,
      contextUrl: resultData.image.contextLink,
      title: resultData.title,
      width: resultData.image.width,
      height: resultData.image.height
    };
  });
}

function createImageNode(imageData) {
  let result = createNode("div");
  result.classList.add("caret");

  let imageLink = createNode("a");
  imageLink.href = imageData.url;
  imageLink.classList.add("image");

  let image = createNode("img");
  image.src = imageData.thumbnailUrl;
  image.alt = imageData.title;

  let contextWebsite = createNode("div");
  contextWebsite.classList.add("description");

  let title = createNode("a");
  title.innerHTML = imageData.title;
  title.href = imageData.contextUrl;

  let dimensions = createNode("p");
  dimensions.innerHTML = `${imageData.width} x ${imageData.height}`;

  imageLink.append(image);
  result.append(imageLink);
  contextWebsite.append(title);
  contextWebsite.append(dimensions);
  result.append(contextWebsite);
  return result;
}

function imgPrevPage() {
  if (imgStart - IMG_RESULTS_CNT <= IMG_RESULTS_CNT)
    addElementClass(".img-results > .pagination > .prev-button", "invisible");

  if (elementHasClass(".img-results > .pagination > .next-button", "invisible"))
    removeElementClass(
      ".img-results > .pagination > .next-button",
      "invisible"
    );

  imgStart -= IMG_RESULTS_CNT;
  imgPageNumber.innerText = `${+imgPageNumber.innerText - 1}`;
  cleanChildNodes(imgResultsElement);
  getImages();
}

function imgNextPage() {
  if (imgStart <= 1)
    removeElementClass(
      ".img-results > .pagination > .prev-button",
      "invisible"
    );

  imgStart += IMG_RESULTS_CNT;
  imgPageNumber.innerText = `${+imgPageNumber.innerText + 1}`;
  cleanChildNodes(imgResultsElement);
  getImages();
}

/* ==========================================================================
  Helper functions
  ========================================================================== */

function createNode(element) {
  return document.createElement(element);
}

const cleanChildNodes = parent => {
  while (parent.firstChild) parent.firstChild.remove();
};

function removeElementClass(elementSelector, elementClass) {
  let element = document.querySelector(elementSelector);
  element.classList.remove(elementClass);
}

function addElementClass(elementSelector, elementClass) {
  let element = document.querySelector(elementSelector);
  element.classList.add(elementClass);
}

function elementHasClass(elementSelector, elementClass) {
  return document
    .querySelector(elementSelector)
    .classList.contains(elementClass);
}
